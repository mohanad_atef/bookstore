@extends('Layout.app_member')
@section('content')
    <div class="container">
        <form action="{{url('book/login')}}" method="POST">
            {{csrf_field()}}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : "" }}">
                email : <input type="text" value="{{Request::old('email')}}" class="form-control" name="email" placeholder="Enter You email">
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : "" }}">
                password : <input type="password" value="{{Request::old('password')}}" class="form-control" name="password" placeholder="Enter You password">
            </div>
            <input type="submit" class="btn btn-primary" value="login ">
        </form>
    </div>
@endsection