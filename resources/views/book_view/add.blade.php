@extends('Layout.app_member')
@section('content')
    <div class="container">
        <form action="{{url('book/add')}}" method="POST">
            {{csrf_field()}}
            <div class="form-group{{ $errors->has('TittelBook') ? ' has-error' : "" }}">
                TittelBook : <input type="text" value="{{Request::old('TittelBook')}}" class="form-control" name="TittelBook" placeholder="Enter You Tittel">
            </div>
            <div class="form-group{{ $errors->has('PostBook') ? ' has-error' : "" }}">
                PostBook : <input type="text" value="{{Request::old('PostBook')}}" class="form-control" name="PostBook" placeholder="Enter You Post">
            </div>
            <input type="submit" class="btn btn-primary" value="Add ">
        </form>
    </div>
@endsection



