@extends('Layout.app_member')
@section('content')
    <div class="container">
        <form action="{{url('book/edit/'.$book->id)}}" method="POST">
            {{csrf_field()}}
            <div class="form-group{{ $errors->has('TittelBook') ? ' has-error' : "" }}">
                TittelBook : <input type="text"  class="form-control" name="TittelBook" value="{{$book->TittelBook}}" placeholder="enter you tittel">
            </div>
            <div class="form-group{{ $errors->has('PostBook') ? ' has-error' : "" }}">
                PostBook : <input type="text" class="form-control" name="PostBook" value="{{$book->PostBook}}" placeholder="enter you post">
            </div>
            <input type="submit" class="btn btn-primary" onclick="return confirm('Are you sure?')" value="Edit Product">
        </form>
    </div>
@endsection