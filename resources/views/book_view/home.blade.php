@extends('Layout.app')
@section('content')
<div class="container">
    <div class="row mt-2">
        <div class="col-md-9 offset-md-2">
            <div class="card mb-3" style="min-width: 18rem;">

                @foreach($book as $mybook)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{$mybook->id}}.{{$mybook->TittelBook}}
                        </div>
                        <div class="panel-body">
                            {{$mybook->PostBook}}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
