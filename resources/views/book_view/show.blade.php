@extends('Layout.app_member')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="box-header pull-left">
                    <h3 class="box-title">ِAll Post</h3>
                </div>
            </div>
        </div>
        <table id="example2" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>id</th>
                <th>TittelBook</th>
                <th>PostBook</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($book as $mybook)
                <tr>
                    <td>{{$mybook->id}}</td>
                    <td>{{$mybook->TittelBook}}</td>
                    <td>{{$mybook->PostBook}}</td>
                    <td>
                        <a href="{{url('book/edit/'.$mybook->id)}}" class="btn btn-primary">edit</a>
                    </td>
                    <td>
                        <a href="{{url('book/delete/'.$mybook->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure?')">delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
@section('footer')
    <script>
        $(function () {
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true,
            })
        })
    </script>
@endsection