@extends('Layout.app_member')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="box-header pull-left">
                    <h3 class="box-title">ِAll member</h3>
                </div>
            </div>
        </div>
        <table id="example2" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>id</th>
                <th>email</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($member as $mymember)
                <tr>
                    <td>{{$mymember->id}}</td>
                    <td>{{$mymember->email}}</td>
                    <td>
                        <a href="{{url('book/member/delete/'.$mymember->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure?')">delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
@section('footer')
    <script>
        $(function () {
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true,
            })
        })
    </script>
@endsection