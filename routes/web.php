<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
//login
Route::get('book/login','MemberController@loginbook');
Route::Post('book/login','MemberController@loginbook');
//home
Route::get('/book/home','bookController@homebook');
//home member
Route::get('/book/home/member','bookController@memberbook');
//add Post
Route::get('/book/add','BookController@addbook');
Route::Post('/book/add','BookController@addbook');
//show post
Route::get('/book/show','BookController@showbook');
//delete post
Route::get('/book/delete/{id}','BookController@deletebook');
//edit post
Route::get('/book/edit/{id}','BookController@editbook');
Route::Post('/book/edit/{id}','BookController@editbook');
//add member
Route::get('/book/member/add','MemberController@addmember');
Route::Post('/book/member/add','MemberController@addmember');
//show member
Route::get('/book/member/show','MemberController@showmember');
//delete member
Route::get('/book/member/delete/{id}','MemberController@deletemember');

