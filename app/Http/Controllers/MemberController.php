<?php

namespace App\Http\Controllers;
use App\Member;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function loginbook(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'email'=>'required|exists:members',
                'password'=>'required|exists:members',
            ]);
            return redirect('book/home/member')->with('message', 'welcome!');
        }
        else
        {
            return view('auth.login');
        }
    }
    //to add member
    public function addmember(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'email'=>'required|unique:members',
                'password'=>'required',
            ]);
            $newmember=new Member();
            $newmember->email = $request->input('email');
            $newmember->password = $request->input('password');
            $newmember->save();
            return redirect('book/home/member')->with('message', 'Add new member Is Done!');
        }
        else
        {
            return view('member_view.add');
        }
    }
    //to show all product
    public function showmember()
    {
        $member=member::all();
        $arr=Array('member'=>$member);
        return view('member_view.show',$arr);
    }
    //to delete book
    public function deletemember($id)
    {
        $member=member::find($id);
        $member->delete();
        return redirect()->back()->with('message', 'Delete member Is Done!');
    }
}
