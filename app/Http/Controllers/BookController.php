<?php

namespace App\Http\Controllers;

use App\Book;
use App\member;
use Illuminate\Http\Request;

class BookController extends Controller
{
    //mean page
    public function homebook()
    {
        $book=Book::all();
        $arr=Array('book'=>$book);
        return view('book_view.home',$arr);
    }
    //mean page
    public function memberbook()
    {
        $book=Book::all();
        $arr=Array('book'=>$book);
        return view('book_view.member_home',$arr);
    }

    //to add Post
    public function addbook(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'TittelBook'=>'required|unique:books',
                'PostBook'=>'required',
            ]);
            $newbook=new Book();
            $newbook->TittelBook = $request->input('TittelBook');
            $newbook->PostBook = $request->input('PostBook');
            $newbook->save();
            return redirect('book/home/member')->with('message', 'Add Post Is Done!');
        }
        else
        {
            return view('book_view.add');
        }
    }
    //to show all product
    public function showbook()
    {
        $book=Book::all();
        $arr=Array('book'=>$book);
        return view('book_view.show',$arr);
    }
    //to edit book
    public function editbook(Request $request,$id)
    {
        if ($request->isMethod('post'))
        {
            $this->validate($request,[
                'TittelBook'=>'required',
                'PostBook'=>'required',
            ]);
            $newbook = book::find($id);
            $newbook->TittelBook = $request->input('TittelBook');
            $newbook->PostBook = $request->input('PostBook');
            $newbook->save();
            return redirect()->back()->with('message', 'Edit post Is Done!');
        }
        else
        {
            $book = book::find($id);
            $arr = Array('book' => $book);
            return view('book_view.edit', $arr);
        }
    }
    //to delete book
    public function deletebook($id)
    {
        $book=book::find($id);
        $book->delete();
        return redirect()->back()->with('message', 'Delete post Is Done!');
    }
}
